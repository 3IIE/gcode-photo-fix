#!/usr/env/python3
""" This file attempts to fix the x & y coordinates that have a specific
comment by replacing those coordinates with coordinates from the previous G0's
X and Y coordinates.

  Usage:

    python gcodePhotoFix.py your_file.gcode

  You can do multiple files using the -b or --batch flag:

    python gcodePhotoFix.py --batch file_1.gcode ile_2.gcode file_3.gcode

 This program does not know where to start the X and Y coordinates. It defaults
 to 0.0, 0.0. If you would like to override the default starting X or Y you can
 specify using one or both:

    startX     Starting X.

    StartY     Starting Y.

    Usage:

        python gcodePhotoFix.py startX=30.5 startY=4.268 your_file.gcode

  Created by Austin Byron,
             austin@3ch0peak.com\n"""


from sys import argv as sArgv, exit as sExit
from os.path import splitext as oSplitText, exists as oExists
from re import compile as rCompile, sub as rSub
from datetime import datetime as dTime
from pygcode.exceptions import GCodeWordStrError
from pygcode import Line


class gPhotoFix():
    """This class will read a gcode file and try to fix the X & Y coordinates
inside a TakePhoto routine."""

    def __init__(self, *args, **kargs):
        """\n  Class has two modes. Single file, batch files processing.

      args: Arguments list,

        -h
        --help          Show this help menu.

        -v
        --verbose       Show all errors.

        -t
        --time-it       Time the script's runtime. If in batch mode will time
                        every file passed to the script.

        -b
        --batch         Do multiple files in a row by passing one of these
                        flags and one or more gcode files.

        [File]          Your file, or files.


      kargs: Keyword argument dictionary object

        startX:         startX=5.436
                        Your very first X coordinate. Defaults to 0.


        startY:         startY=12.3
                        Your very first Y coordinate. Defaults to 0.\n"""


        ## setup our kargs if there are any?

        ## our X karg
        if 'startX' in kargs.keys():
            self.startX = float(kargs['startX'])
        else:
            self.startX = float(0)

        ## our Y karg
        if 'startY' in kargs.keys():
            self.startY = float(kargs['startY'])
        else:
            self.startY = float(0)

        ## Compile our X and Y regex expression's.
        self.__xRgx = rCompile(r"X[0-9]{1,}\.?[0-9]*")
        self.__yRgx = rCompile(r"Y[0-9]{1,}\.?[0-9]*")

        self.arg_setup(args)

        ## Look for if only one argument, that argument is a file and that the
        ## file exists.
        if len(args) == 1 and oExists(args[0]):
            ## assign the location to the file variable
            self.file = args[0]
            ## read and fix the gcode and store it in the gcode variable
            self.gcode = self.readNfix()
            ## save the gcode.
            self.saveGcode()

    def arg_setup(self, args:list):
        """ """

        ## Setup our args if there are any?

        ## Look for help in args
        if 1 in (1 for x in ('-h', '--help') if x in args):
            ## -h, --help was in args so we will now print the init's
            ## docstring and exit
            sExit(self.__init__.__doc__)

        # set the time-it variable to False to start out
        timeIt = False

        ## find out if we are timing the script execution's?
        if 1 in (1 for x in ('-t', '--time-it') if x in args):
            timeIt = True
            starTimeTotal = dTime.now()
            args += ('-b',)

        ## set the verbose variable to False to start
        verbose = False

        ## find out if user wants errors printed.
        if 1 in (1 for x in ('-v', '--verbose') if x in args):
            verbose = True
            args += ('-b',)

        ## Look for batch files flag
        if 1 in (1 for x in ('-b', '--batch') if x in args) and len(args) > 1:
            ## Loop through all the args.
            for loc in args:
                ## if the location exists we need to
                if oExists(loc):

                    ## Ask if we are timing our script? We need to do this for
                    ## each file in our loop.
                    if timeIt:
                        ## we are timing our script. Set our starTime to
                        ## datetime.now()
                        starTime = dTime.now()

                    ## assign the new location to the file variable
                    self.file = loc
                    ## Print filename so users know whats going on if errors
                    ## pop up.
                    line = '_________________________________'
                    print(f'{line}\n\n>> File: {self.file}\n')
                    ## read and fix the gcode and store it in the gcode
                    ## variable
                    self.gcode = self.readNfix(verbose)
                    ## save the gcode.
                    self.saveGcode()

                    ## ask if we are timing our script?
                    if timeIt:
                        ## we are timing our script so we need to tell the
                        ## world.
                        say = f"-->  The file {self.file} was fixed in "
                        say += f'{dTime.now() - starTime}\n'
                        print(say)
            ## if we are timing the script we need to print our total elapsed
            ## time since start of script.

            if timeIt:
                line = '__________'
                say = f"\n{line}  Elapsed time {dTime.now() - starTimeTotal}. "
                say += f" {line}\n"
                print(say)


    def readNfix(self, mkPrint=False):
        """This method will read a gcode file reading and storing the last read
G00 X's & Y's. Those X & Y coordinates are then used to replace the next
string's coordinates that has the comment string "return to basic area of print
to minimize stringing" in it.

        It then returns a string with all the changes made. """

        ## Define the dictionary we will hold our last read coordinate in.
        lastCoordinate = {'X': self.startX, 'Y': self.startY}

        ## Define the string we are going to return from this function.
        saveStr = ''

        ## Setup an error reporting dictionary
        errors = {}

        ## Here we are going to read from our file.
        with open(self.file, 'r') as fh:
            ## We need to go line by line so we are going to use the opened
            ## files readlines(), we also need a line number in case something 
            ##  goes wrong we can have a record where. For that we use
            ## enumerate() to count our loop iterations.
            for lineNum, line_text in enumerate(fh.readlines(), 1):
                ## Try, except are advanced concepts called error handling. We
                ## will try to do:
                try:
                    ## Here we try to parse our gcode line with pygcode.
                    line = Line(line_text)
                except GCodeWordStrError as e:
                    ## We found an error. Really it's not that big of a deal.
                    ## We are going to add the text to our saveStr string
                    ## object.
                    saveStr += line_text

                    ## we need to store our errors we find. We will do this
                    ## here.
                    if line_text in errors.keys():
                        ## our errors dictionary has this error already we just
                        ##  need to append the line number to the second value
                        ## in the list.
                        errors[line_text][1].append(lineNum)
                    else:
                        ## our errors dictionary does not have this error in it
                        ##  yet so we will build it now.
                        errors[line_text] = [e, [lineNum]]

                else:
                    ## So our gcode line was parsed successfully.
                    ## Here we will test if our gcode has both an X and a Y.
                    if (len(line.gcodes) >= 1 and
                        [1, 1] == [1 for x in ['X', 'Y'] if
                                   x in line.gcodes[0].get_param_dict()]):
                        ## We have both an X and a Y.

                        ## Now we will get our gcode lines word_key string
                        wordKey = str(line.gcodes[0].word_key)

                        ## We then use that wordKey to see if it is equal to
                        ## G00
                        if wordKey == 'G00':
                            ## Found for GCodeLinearMove
                            ## We can now set our lastCoordinate variable to
                            ## the gcodes line.get_param_dict()
                            lastCoordinate = line.gcodes[0].get_param_dict()

                    ## Here we test if the gcode line has the comment we want.
                    line_comment = 'return to basic area of print to minimize '
                    line_comment += 'stringing'
                    if line.comment and line.comment.text == line_comment:
                        ## We found a line we want to replace so we send it
                        ## through our gcodeReplaceCoord function and add it to
                        ## the saveStr string object.
                        saveStr += self.__gcodeReplaceCoord(lastCoordinate,
                                                            line_text)
                    else:
                        ## This gcode line does not have that comment at all.
                        ## We still want to add it though.
                        saveStr += line_text

        ## ask if the file is supposed to print out errors in verbose mode?
        if mkPrint and errors != {}:
            ## we are printing errors so we need to give the world what they
            ## asked for.
            for lineText, vals in errors.items():
                error = f"  An error occurred: {vals[0]}, {len(vals[1])} times"
                error += f"on lines:\n     {vals[1]}\n"
                error += '  This can likely safely be ignored. Just means the '
                error += 'line was not parseable by pygcode.'
                error += 'he Line text that caused the failure was:\n'
                error += f"       {lineText}"
                print(error)

        ## Ok We have read and built our new file text. We can now return it
        ## from this function.
        return saveStr


    ## Here we will define this classes save method. It will save our gcode
    ## string.
    def saveGcode(self, nDif='_fix'):
        """ This method will save the new gcode in the same folder the original
        gcode file is in. The file will be named the same but _fix.gcode will
        be appended to the new file name. """

        ## oSplitText is a filename object split at its dot extension dot. We
        ## use this to create our new fileName easily.
        sText = oSplitText(self.file)

        ## Now we write our new gcode text to our new gcode file
        with open(f'{sText[0]}{nDif}{sText[1]}', 'w') as fh:
            fh.write(self.gcode)


    ## Here we define the method that will replace the X and Y
    def __gcodeReplaceCoord(self, repObj, srchStr):
        """ This method will search and replace a gcode strings X & Y """

        ## Loop through the replacement X and Y to fix the string.
        for key, val in repObj.items():

            ## Check to see if our current key is X
            if key == 'X':
                ## our key is X so we will use regex rSub to replace our X
                srchStr = rSub(self.__xRgx, f'{key}{val}', srchStr)
            else:
                ## our key is Y so we will use regex rSub to replace our Y
                srchStr = rSub(self.__yRgx, f'{key}{val}', srchStr)

        ## Now that we have replaced both our X and our Y we can return our new
        ## string.
        return srchStr


## Here we ask our python interpreter if our file is being run as a file or
## imported as a module.
if __name__ == '__main__':
    ## Python says that we are running as a file and not being imported as a
    ## module.
    try:
        ## Here we try to setup our program and run it.

        sep = ('=', ':')

        ## This is a advanced concept called list comprehension. It will sort
        ## the single arguments into a list.
        arg = [x for x in sArgv[1:] if '=' not in x]

        ## This is another advanced concept called dictionary comprehension. It
        ## will sort all the keyword arguments into a dictionary.

        ## Windows should now be fully supported? I cant test but I can see no
        ## reason why it wouldn't with the added windows friendly deliminator
        ## of ':' .
        karg = {x.split(y, 1)[0]: x.split(y, 1)[1] for x in sArgv[1:]
                                                    for y in sep if y in x}

        ## Now that we have our args and kargs set up we can feed them into our
        ## gPhotoFix() class.
        gPhotoFix(*arg, **karg)

    except KeyboardInterrupt as e:
        ## I designed in a keyboard interrupt using [Ctrl] + c. You can use it
        ## to exit your program if it is taking to much time. These file are
        ## kind of large and might take a while to parse. Thought I'd give you
        ## an easy way out.

        ## sExit will system exit
        sExit(e)
