#!/usr/env/python3.7
""" This file while in mostly working condition it is not intended to be used.
It was me learning the pygcode module. It also suffers from me not fully 
understanding what the person I designed this for really needed done and as 
such I have made this file more complicated than necessary. Im leaving it in 
the repo as more code that can be referenced. It possibly has value beyond it's 
lack of usefulness?


This file attempts to fix the x & y coordinates that are at the beginning of 
a slice by replacing those coordinates with coordinates from the previous
layers last coordinates.

    Usage:

        python3.7 gcodePostProcessor.py your_file.gcode 


  You can do multiple files using the -b or --batch flag:

 python3.7 gcodePostProcessor.py --batch ile_1.gcode file_2.gcode file_3.gcode
 """


from sys import argv as sArgv, exit as sExit
from os.path import splitext as oSplitText, exists as oExists
from re import compile as rCompile, sub as rSub
from pygcode.exceptions import GCodeWordStrError
from pygcode import Line


class gSlice():
    """  This is a base class for others to inherit from. It contains basic
    methods to read, parse and return a sliced dictionary object. """

    def __init__(self, *args, **kargs):
        """ Creates a sliced object from a file. """

        if len(args) == 1 and oExists(args[0]):
            self.file = args[0]
            self.slices = self.mkGcodeLineObj()
        else:
            self.file = None
            self.slices = None


    def mkGcodeLineObj(self):
        """This method will read a gcode file and try to split and return the 
gcode into a dictionary object. The keys will be labeled slice_X where X is
your slice number. The values will hold an array of gcode file lines.  """

        gcodeDict = {}
        sliceCount = 0
        gLines = []

        with open(self.file, 'r') as fh:

            for line_text in fh.readlines():
                try:
                    line = Line(line_text)
                except GCodeWordStrError as e:
                    gLines.append(line_text)
                    print(f"""  An error occurred:   {e}
  This can likely safely be ignored. Just means the line was not parseable by pygcode.
  The Line text that caused the failure was:\n       {line_text}""")
                else:
                    if line.comment and line.comment.text == 'MESH:Pedestal_Assembly.stl':
                        gLines.append(line_text)
                        sliceCount += 1
                        gcodeDict.update({f'slice_{sliceCount}': gLines})
                        gLines = []
                    else:
                        gLines.append(line_text)
            else:
                sliceCount += 1
                gcodeDict.update({f'slice_{sliceCount}': gLines})
                    
        return gcodeDict


    def saveGcode(self, nDif='_fix'):
        """ This method will save the new gcode in the same folder the original
        gcode file is in. The file will be named the same but _fix.gcode will 
        be appended to the new file name. """

        gCodeStr = ''
        sText = oSplitText(self.file)

        for key, slce in self.slices.items():
            gCodeStr += ''.join(slce)
        with open(f'{sText[0]}{nDif}{sText[1]}', 'w') as fh:
            fh.write(gCodeStr)


class gSliceN_fixXY(gSlice):
    """This class attempts to fix the x & y coordinates that are at the  
beginning of a slice by replacing those coordinates with coordinates from the
previous layers last coordinates. """

    def __init__(self, *args, **krgs):
        """\n  Class has three modes. Single file, batch files processing
  and testing(--wait)

        -h
        --help     Show this help menu.

        -b
        --batch    Do multiple files in a row by passing one of these
                   flags and one or more gcode files.

        -w
        --wait     Only one file can be passed. This class will wait for you
                   to call self.saveGcode() before the input file will be
                   processed or saved. Used for testing.\n """

        super(gSliceN_fixXY, self).__init__(args, krgs)

        self.xRgx = rCompile(r"X[0-9]{1,}\.?[0-9]*")
        self.yRgx = rCompile(r"Y[0-9]{1,}\.?[0-9]*")

        if 1 in (1 for x in ['-h', '--help'] if x in args):
            sExit(self.__init__.__doc__)

        if 1 in (1 for x in ['-b', '--batch'] if x in args) and len(args) > 1:
            for loc in args:
                if oExists(loc):
                    self.file = loc
                    self.slices = self.mkGcodeLineObj()
                    self.saveGcode()

        if 1 in (1 for x in ['-w', '--wait'] if x in args) and len(args) < 3:
            self.file = [x for x in args if x not in ['-w', '--wait']][0]
            self.file = self.file if oExists(self.file) else None
            if self.file is not None:
                self.slices = self.mkGcodeLineObj()
            else:
                self.slices = None


        if len(args) == 1 and oExists(args[0]):
            self.file = args[0]
            self.slices = self.mkGcodeLineObj()
            self.saveGcode()
 

    def fixCoord(self, gObj):
        """This method loops through each slice of the given gObj and updates
that slices list x & y values to the previous layers last known x & y
coordinates. Will return an updated dictionary object."""

        lastCoords = None
        retDict = {}

        for slicdG, gList in gObj.items():
            if not slicdG == 'slice_1':
                
                sliceListFix = self.replaceFirstCoord(lastCoords, gList)

                # print(f'new list:\n{sliceListFix[:5]}')

                retDict[slicdG] = sliceListFix

                lastCoords = self.getLastCoord(gList)
                #return 1

            else:
                ##!! This is the first slice  !!##
                lastCoords = self.getLastCoord(gList)
                retDict[slicdG] = gList
                # return 1

        return retDict


    def replaceFirstCoord(self, repCoord, coordList):
        """This method looks through the slice for the first gcode line that
has x & y values and replaces that line with the correct x & y values. Then  
returns the new gcode list. """

        firstCoord = None
        firstCoordIndex = None
        for i, gCode in enumerate(coordList):
            try:
                line = Line(gCode)
            except GCodeWordStrError:
                ## if this exception were to come up it will already have been
                ## reported during the initial mkGcodeLineObj() call. All
                ## errors can safely be ignored.
                pass
            else:
                if (len(line.gcodes) >= 1 and
                    [1, 1] == (1 for x in ()'X', 'Y') if
                               x in line.gcodes[0].get_param_dict().keys()):
                    ## We now know that our gCode has a X and Y
                    firstCoordIndex = i
                    firstCoord = line
                    break

        # print(f'Index: {firstCoordIndex}\nFirst line with X and Y: {firstCoord}')

        fixedCoord = self.gcodeReplaceCoord(repCoord, firstCoord.block.text)

        # print(f'Fixxed coordinate: {fixedCoord}')

        coordList[firstCoordIndex] = f'{fixedCoord}\n'

        return coordList


    def getLastCoord(self, gObj):
        """This method will look for and return the last X & Y coordinate in a
slice."""

        for gCode in reversed(gObj):
            try:
                line = Line(gCode)
            except GCodeWordStrError:
                ## if this exception were to come up it will already have been
                ## reported during the initial mkGcodeLineObj() call. All
                ## errors can safely be ignored.
                pass
            else:
                if (len(line.gcodes) >= 1 and
                    [1, 1] == (1 for x in ('X', 'Y') if 
                               x in line.gcodes[0].get_param_dict().keys())):
                    ## We now know that our gCode has a X and Y

                    wordKey = str(line.gcodes[0].word_key)

                    if wordKey in ['G00', 'G01']:
                        ## Found for GCodeLinearMove
                        return line.gcodes[0].get_param_dict()


    def gcodeReplaceCoord(self, repObj, srchStr):
        """ This method will search and replace a gcode strings X & Y """

        # print(repObj, srchStr)
        for key, val in repObj.items():

            if key == 'X':
                srchStr = rSub(self.xRgx, f'{key}{val}', srchStr)
            else:
                srchStr = rSub(self.yRgx, f'{key}{val}', srchStr)

        return srchStr


    def saveGcode(self, nDif='_XYfix'):
        """ This method will fix then save the new gcode the the same folder
the original gcode file is in. The file will be named the same but _XYfix.gcode 
 will be appended to the new file name. """

        self.slices = self.fixCoord(self.slices)

        super(gSliceN_fixXY, self).saveGcode(nDif=nDif)


if __name__ == '__main__':
    try:
        gSliceN_fixXY(*sArgv[1:])
    except KeyboardInterrupt as e:
        sExit(e)
