# Gcode Photo Fix



This file attempts to fix the x & y coordinates that have a specific
comment by replacing those coordinates with coordinates from the previous G0's
X and Y coordinates.

## Usage:

    python gcodePhotoFix.py your_file.gcode

## You can do multiple files using the -b or --batch flag:

    python gcodePhotoFix.py --batch file_1.gcode ile_2.gcode file_3.gcode

 This program does not know where to start the X and Y coordinates. It defaults
 to 0.0, 0.0. If you would like to override the default starting X or Y you can
 specify using one or both:

    startX     Starting X.

    StartY     Starting Y.

## Usage:

        python gcodePhotoFix.py startX=30.5 startY=4.268 your_file.gcode

  Created by Austin Byron,
             austin@3ch0peak.com

This little project I wrote up knowing nothing about gcode. My sisters coworker
had a 3D printer and wanted to take pictures every time the print went up on 
it's z axis to make like a stop motion animation of his prints. It was my first
 code I made for someone other than myself.